/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.demo

import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.client.DialerNotificationListenerService
import kotlinx.coroutines.flow.MutableSharedFlow

class DialerService: DialerNotificationListenerService() {
    companion object {
        val updateNotificationChannel = MutableSharedFlow<Unit>(replay = 1)
    }

    override fun onCallUpdated(callInfo: CallInfo) {
        updateNotificationChannel.tryEmit(Unit)
    }

    override fun onCallEnded(callInfo: CallInfo) {
        updateNotificationChannel.tryEmit(Unit)
    }
}