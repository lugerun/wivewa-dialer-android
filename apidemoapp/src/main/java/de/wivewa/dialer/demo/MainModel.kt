/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.demo

import android.app.Application
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.widget.RemoteViews
import androidx.compose.material3.SnackbarHostState
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.client.SimpleDialerClient
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine

class MainModel(application: Application): AndroidViewModel(application) {
    companion object {
        private val handler = Handler(Looper.getMainLooper())
    }

    sealed class State {
        object UnsupportedDialer: State()
        object MissingPermission: State()
        object CommunicationError: State()
        class Connected(val calls: List<CallInfo>): State()
    }

    private val intent = DemoActivity.pendingIntent(application)

    private val client = SimpleDialerClient(application, handler).also { it.start() }

    private val update = MutableStateFlow(0)
    fun update() { update.update { it + 1 } }

    init {
        viewModelScope.launch {
            DialerService.updateNotificationChannel.collect { update() }
        }
    }

    val requestPermissionIntent: Intent
        get() = client.requestPermissionIntent()

    val snackbar = SnackbarHostState()

    val state: Flow<State> = update.transformLatest {
        try {
            val calls = suspendCancellableCoroutine { result ->
                client.getCurrentCalls {
                    result.resumeWith(it)
                }
            }

            emit(State.Connected(calls))
        } catch (ex: SecurityException) {
            emit(State.MissingPermission)
        } catch (ex: SimpleDialerClient.UnsupportedDialerException) {
            emit(State.UnsupportedDialer)
        } catch (ex: Exception) {
            emit(State.CommunicationError)
        }
    }

    fun assignName(callInfo: CallInfo) {
        viewModelScope.launch {
            val name = CallerNames.pick()

            try {
                assign(callInfo, CallExtras().also { it.displayName = name })

                snackbar.showSnackbar(name)
            } catch (ex: Exception) {
                snackbar.showSnackbar(getApplication<Application>().getString(R.string.communication_error))
            }
        }
    }

    fun assignCallerAction(callInfo: CallInfo) {
        viewModelScope.launch {
            try {
                assign(callInfo, CallExtras().also { it.callerDetailsAction = intent })
            } catch (ex: Exception) {
                snackbar.showSnackbar(getApplication<Application>().getString(R.string.communication_error))
            }
        }
    }

    fun assignView(callInfo: CallInfo, res: Int) {
        viewModelScope.launch {
            try {
                assign(callInfo, CallExtras().also { it.callerDetailsView = RemoteViews(
                    getApplication<Application>().packageName, res
                ) })
            } catch (ex: Exception) {
                snackbar.showSnackbar(getApplication<Application>().getString(R.string.communication_error))
            }
        }
    }

    private suspend fun assign(callInfo: CallInfo, callExtras: CallExtras) {
        suspendCancellableCoroutine { result ->
            client.enhanceCall(callInfo.id, callExtras) { result.resumeWith(it) }
        }
    }

    override fun onCleared() {
        super.onCleared()

        client.stop()
    }
}