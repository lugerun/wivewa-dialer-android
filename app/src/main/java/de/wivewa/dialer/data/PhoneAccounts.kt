/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.data

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.telecom.PhoneAccount
import android.telecom.TelecomManager
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.flow

object PhoneAccounts {
    @SuppressLint("MissingPermission")
    fun get(context: Context, telecomManager: TelecomManager) = flow<List<PhoneAccount>> {
        val notification = Channel<Unit>(Channel.CONFLATED)

        val receiver = object: BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent?) {
                notification.trySend(Unit)
            }
        }

        try {
            context.registerReceiver(receiver, IntentFilter(TelecomManager.ACTION_PHONE_ACCOUNT_REGISTERED))
            context.registerReceiver(receiver, IntentFilter(TelecomManager.ACTION_PHONE_ACCOUNT_UNREGISTERED))

            while (true) {
                emit(
                    telecomManager.callCapablePhoneAccounts.map {
                        telecomManager.getPhoneAccount(it)
                    }
                )

                notification.receive()
            }
        } finally {
            context.unregisterReceiver(receiver)
        }
    }
}