/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import androidx.core.content.getSystemService
import de.wivewa.dialer.R

object NotificationChannels {
    const val RINGING_CALL = "ringing call"
    const val RINGING_CALL_LOW = "ringing call lower priority"
    const val CURRENT_CALL = "current call"

    fun create(context: Context) {
        val notificationManager = context.getSystemService<NotificationManager>()!!

        notificationManager.createNotificationChannels(listOf(
            NotificationChannel(
                RINGING_CALL,
                context.getString(R.string.notification_channel_ringing_call_title),
                NotificationManager.IMPORTANCE_HIGH
            ).also { channel ->
                channel.setSound(null, null)
                channel.vibrationPattern = null
            },
            NotificationChannel(
                RINGING_CALL_LOW,
                context.getString(R.string.notification_channel_ringing_call_low_title),
                NotificationManager.IMPORTANCE_DEFAULT
            ).also { channel ->
                channel.setSound(null, null)
                channel.vibrationPattern = null
            },
            NotificationChannel(
                CURRENT_CALL,
                context.getString(R.string.notification_channel_current_call_title),
                NotificationManager.IMPORTANCE_DEFAULT
            ).also { channel ->
                channel.setSound(null, null)
                channel.vibrationPattern = null
            }
        ))
    }
}