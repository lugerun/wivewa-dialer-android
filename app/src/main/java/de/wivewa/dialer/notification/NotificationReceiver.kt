/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.notification

import android.content.Context
import android.content.Intent
import de.wivewa.dialer.platform.AsyncBroadcastReceiver
import de.wivewa.dialer.service.CallInfo
import de.wivewa.dialer.service.CallService
import kotlinx.coroutines.flow.firstOrNull

class NotificationReceiver: AsyncBroadcastReceiver() {
    companion object {
        const val ACTION_REJECT_RINGING_CALL = "reject"
        const val ACTION_MUTE_RINGING_CALL = "mute ring"
        const val ACTION_DISCONNECT_CURRENT_CALL = "disconnect"
    }

    override suspend fun handleReceive(context: Context, intent: Intent) {
        when (intent.action) {
            ACTION_REJECT_RINGING_CALL -> CallService.currentCalls.firstOrNull()
                ?.let { calls -> CallInfo.getRinging(calls) }
                ?.let { ringing -> ringing.actions.reject() }
            ACTION_MUTE_RINGING_CALL -> CallService.currentCalls.firstOrNull()
                ?.let { calls -> CallInfo.getRinging(calls) }
                ?.let { ringing -> ringing.actions.silenceRinger() }
            ACTION_DISCONNECT_CURRENT_CALL ->
                CallService.currentCalls.firstOrNull()
                    ?.let { calls -> CallInfo.getCurrent(calls) }
                    ?.let { current -> current.actions.disconnect() }
        }
    }
}