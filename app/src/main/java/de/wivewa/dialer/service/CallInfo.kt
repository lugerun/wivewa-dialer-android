/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.service

import android.app.PendingIntent
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.telecom.Call
import android.telecom.PhoneAccountHandle
import android.telecom.PhoneAccountSuggestion
import android.telecom.TelecomManager
import android.widget.RemoteViews
import de.wivewa.dialer.platform.CallExtras

data class CallInfo(
    val id: String?,
    val state: Int,
    val hasParent: Boolean,
    val hasChildren: Boolean,
    val canMerge: Boolean,
    val phoneAccountSuggestions: List<PhoneAccountSuggestion>,
    val remotePhoneNumber: String?,
    val remoteDisplayName: String?,
    val silenceRinging: Boolean,
    val integrationDisplayName: String?,
    val integrationDetailsAction: PendingIntent?,
    val integrationView: RemoteViews?,
    val actions: Actions
) {
    data class Actions(
        val answer: () -> Unit,
        val reject: () -> Unit,
        val silenceRinger: () -> Unit,
        val playDtmfTone: (Char) -> Unit,
        val hold: () -> Unit,
        val unhold: () -> Unit,
        val merge: () -> Unit,
        val disconnect: () -> Unit,
        val selectPhoneAccount: (PhoneAccountHandle) -> Unit
    )

    companion object {
        fun fromCall(call: Call, actions: Actions): CallInfo {
            val state = if (VERSION.SDK_INT >= VERSION_CODES.S) call.details.state else call.state

            return CallInfo(
                id = call.details.intentExtras.getString(CallExtras.CALL_ID, "").ifEmpty { null },
                state = state,
                hasParent = call.parent != null,
                hasChildren = call.children.isNotEmpty(),
                canMerge = call.conferenceableCalls.isNotEmpty(),
                phoneAccountSuggestions = when (state) {
                    Call.STATE_SELECT_PHONE_ACCOUNT ->
                        if (VERSION.SDK_INT >= VERSION_CODES.TIRAMISU) call.details.intentExtras.getParcelableArrayList(Call.EXTRA_SUGGESTED_PHONE_ACCOUNTS, PhoneAccountSuggestion::class.java) as List<PhoneAccountSuggestion>
                        else call.details.intentExtras.getParcelableArrayList<PhoneAccountSuggestion>(Call.EXTRA_SUGGESTED_PHONE_ACCOUNTS) as List<PhoneAccountSuggestion>
                    else -> emptyList()
                },
                remotePhoneNumber = if (
                    call.details.handlePresentation == TelecomManager.PRESENTATION_ALLOWED &&
                    call.details.handle.scheme == "tel"
                ) call.details.handle.schemeSpecificPart else null,
                remoteDisplayName = null,
                silenceRinging = false,
                integrationDisplayName = null,
                integrationDetailsAction = null,
                integrationView = null,
                actions = actions
            )
        }

        fun getRinging(list: Collection<CallInfo>) = list
            .filter { it.state == Call.STATE_RINGING }
            .maxByOrNull {
                when (it.silenceRinging) {
                    true -> 0
                    false -> 1
                }
            }

        fun getCurrent(list: Collection<CallInfo>): CallInfo? = getCurrentAndBackground(list)?.first

        fun getCurrentAndBackground(list: Collection<CallInfo>): Pair<CallInfo, List<CallInfo>>? {
            val primaries = list.filterNot { it.hasParent }

            val maxState = primaries
                .filter { it.state != Call.STATE_RINGING }
                .maxByOrNull {
                    when (it.state) {
                        Call.STATE_HOLDING -> 0
                        Call.STATE_ACTIVE -> 1
                        else -> 2
                    }
                }
                ?.state

            val callsWithMaxState = primaries
                .filter { it.state == maxState }

            val current = callsWithMaxState.firstOrNull() ?: return null
            val background = primaries - current

            return Pair(current, background)
        }
    }
}