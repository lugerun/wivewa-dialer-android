/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.service

import android.annotation.SuppressLint
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.telecom.Call
import android.telecom.CallAudioState
import android.telecom.TelecomManager
import android.util.Log
import androidx.core.content.getSystemService
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.server.service.ConnectedInCallService
import de.wivewa.dialer.BuildConfig
import de.wivewa.dialer.data.SystemContacts
import de.wivewa.dialer.extensions.areBitsSet
import de.wivewa.dialer.extensions.update
import de.wivewa.dialer.notification.*
import de.wivewa.dialer.ui.call.CallActivity
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.*

class CallService: ConnectedInCallService() {
    companion object {
        private const val LOG_TAG = "CallService"

        private val emptyCalls = MutableStateFlow<List<CallInfo>>(emptyList())
        private val currentCallsNested = MutableStateFlow<Flow<List<CallInfo>>>(emptyCalls)
        private val emptyCallAudio = MutableStateFlow(null as CallAudio?)
        private val currentCallAudioNested = MutableStateFlow<Flow<CallAudio?>>(emptyCallAudio)

        @OptIn(ExperimentalCoroutinesApi::class)
        val currentCalls: Flow<List<CallInfo>> = currentCallsNested
            .transformLatest { emitAll(it) }
            .conflate()

        @OptIn(ExperimentalCoroutinesApi::class)
        val callAudio: Flow<CallAudio?> = currentCallAudioNested
            .transformLatest { emitAll(it) }
            .conflate()

        init {
            if (BuildConfig.DEBUG) {
                @OptIn(DelicateCoroutinesApi::class)
                GlobalScope.launch {
                    currentCalls.collect {
                        Log.d(LOG_TAG, "updated call list: $it")
                    }
                }
            }
        }
    }

    internal data class CallMeta(
        val info: CallInfo,
        val scope: CoroutineScope
    )

    private lateinit var scope: CoroutineScope
    private val currentCalls = MutableStateFlow<Map<Call, CallMeta>>(emptyMap())
    private val currentCallInfoList = currentCalls.map { it.values.map { it.info } }
    private val callAudio = MutableStateFlow(null as CallAudio?)
    private lateinit var telecomManager: TelecomManager

    override fun onCreate() {
        super.onCreate()

        NotificationChannels.create(this)

        scope = CoroutineScope(Dispatchers.Main)

        currentCallsNested.value = currentCallInfoList
        currentCallAudioNested.value = callAudio

        telecomManager = getSystemService()!!

        scope.launch { RingingCallNotification.process(this@CallService, currentCallInfoList) }
        scope.launch { CurrentCallNotification.process(this@CallService, currentCallInfoList) }
    }

    override fun onDestroy() {
        super.onDestroy()

        scope.cancel()

        currentCallsNested.value = emptyCalls
        currentCallAudioNested.value = emptyCallAudio
    }

    @SuppressLint("MissingPermission")
    override fun onCallAdded(call: Call) {
        super.onCallAdded(call)

        val scope = CoroutineScope(scope.coroutineContext)

        val dtmfChannel = Channel<Char>(Channel.UNLIMITED)

        scope.launch {
            dtmfChannel.consumeEach {
                call.playDtmfTone(it)
                delay(100)
                call.stopDtmfTone()
            }
        }

        val actions = CallInfo.Actions(
            answer = { call.answer(0) },
            reject = { call.reject(false, null) },
            silenceRinger = {
                currentCalls.update { map ->
                    map.update(call) { meta ->
                        if (meta.info.state == Call.STATE_RINGING) try {
                            telecomManager.silenceRinger()

                            meta.copy(
                                info = meta.info.copy(silenceRinging = true)
                            )
                        } catch (ex: SecurityException) {
                            meta
                        } else meta
                    }
                }
            },
            playDtmfTone = { dtmfChannel.trySend(it) },
            hold = { call.hold() },
            unhold = { call.unhold() },
            merge = {
                call.conferenceableCalls.firstOrNull()?.let { otherCall ->
                    call.conference(otherCall)
                }
            },
            disconnect = { call.disconnect() },
            selectPhoneAccount = { call.phoneAccountSelected(it, false) }
        )

        val callInfo = CallInfo.fromCall(call, actions)
        val callMeta = CallMeta(info = callInfo, scope = scope)

        currentCalls.update { it + Pair(call, callMeta) }

        call.registerCallback(object: Call.Callback() {
            override fun onStateChanged(call: Call, state: Int) {
                super.onStateChanged(call, state)

                currentCalls.update { map ->
                    map.update(call) { meta ->
                        meta.copy(
                            info = meta.info.copy(state = state)
                        )
                    }
                }
            }

            override fun onParentChanged(call: Call, parent: Call?) {
                super.onParentChanged(call, parent)

                currentCalls.update { map ->
                    map.update(call) { meta ->
                        meta.copy(
                            info = meta.info.copy(hasParent = parent != null)
                        )
                    }
                }
            }

            override fun onChildrenChanged(call: Call, children: MutableList<Call>) {
                super.onChildrenChanged(call, children)

                currentCalls.update { map ->
                    map.update(call) { meta ->
                        meta.copy(
                            info = meta.info.copy(hasChildren = children.isNotEmpty())
                        )
                    }
                }
            }

            override fun onConferenceableCallsChanged(call: Call, conferenceableCalls: MutableList<Call>) {
                super.onConferenceableCallsChanged(call, conferenceableCalls)

                currentCalls.update { map ->
                    map.update(call) { meta ->
                        meta.copy(
                            info = meta.info.copy(canMerge = conferenceableCalls.isNotEmpty())
                        )
                    }
                }
            }
        })

        if (callInfo.remotePhoneNumber != null) {
            scope.launch {
                SystemContacts.query(this@CallService, callInfo.remotePhoneNumber)?.also { systemContact ->
                    currentCalls.update { map ->
                        map.update(call) { meta ->
                            meta.copy(
                                info = meta.info.copy(remoteDisplayName = systemContact.displayName)
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onCallExtrasUpdated(call: Call, extras: CallExtras) {
        currentCalls.update { map ->
            map.update(call) { meta ->
                meta.copy(
                    info = meta.info.copy(
                        integrationDisplayName = extras.displayName ?: meta.info.integrationDisplayName,
                        integrationDetailsAction = extras.callerDetailsAction ?: meta.info.integrationDetailsAction,
                        integrationView = extras.callerDetailsView ?: meta.info.integrationView
                    )
                )
            }
        }
    }

    override fun onCallRemoved(call: Call) {
        super.onCallRemoved(call)

        currentCalls.getAndUpdate { it - call }[call]?.scope?.cancel()
    }

    override fun onCallAudioStateChanged(audioState: CallAudioState) {
        super.onCallAudioStateChanged(audioState)

        fun decodeRouteMask(mask: Int, devices: List<BluetoothDevice>): List<CallAudio.Route> {
            val bluetooth = if (mask.areBitsSet(CallAudioState.ROUTE_BLUETOOTH))
                devices.map { CallAudio.Route.Bluetooth(it) }
            else
                emptyList()

            val wiredHeadsetOrEarpiece = mask.areBitsSet(CallAudioState.ROUTE_WIRED_OR_EARPIECE)
            val earpiece = mask.areBitsSet(CallAudioState.ROUTE_EARPIECE)
            val speaker = mask.areBitsSet(CallAudioState.ROUTE_SPEAKER)
            val wiredHeadset = mask.areBitsSet(CallAudioState.ROUTE_WIRED_HEADSET)

            val other = listOfNotNull(
                if (wiredHeadsetOrEarpiece) CallAudio.Route.WiredHeadsetOrEarpiece else null,
                if (wiredHeadset && !wiredHeadsetOrEarpiece) CallAudio.Route.WiredHeadset else null,
                if (earpiece && !wiredHeadsetOrEarpiece) CallAudio.Route.Earpiece else null,
                if (speaker) CallAudio.Route.Speaker else null
            )

            return other + bluetooth
        }

        val currentRoutes = decodeRouteMask(audioState.route, listOfNotNull(audioState.activeBluetoothDevice))
        val availableRoutes = decodeRouteMask(audioState.supportedRouteMask, audioState.supportedBluetoothDevices.toList())

        callAudio.update {
            CallAudio(
                isMuted = audioState.isMuted,
                toggleMute = { setMuted(!audioState.isMuted) },
                currentRoutes = currentRoutes,
                availableRoutes = availableRoutes,
                useAudioRoute = ::useAudioRoute
            )
        }
    }

    override fun onSilenceRinger() {
        super.onSilenceRinger()

        scope.launch {
            currentCallInfoList.firstOrNull()?.let { calls ->
                CallInfo.getRinging(calls)
                    ?.actions
                    ?.silenceRinger
                    ?.invoke()
            }
        }
    }

    override fun onBringToForeground(showDialpad: Boolean) {
        super.onBringToForeground(showDialpad)

        startActivity(
            Intent(this, CallActivity::class.java)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        )
    }

    private fun useAudioRoute(route: CallAudio.Route) {
        when (route) {
            is CallAudio.Route.Bluetooth -> requestBluetoothAudio(route.device)
            CallAudio.Route.Earpiece -> setAudioRoute(CallAudioState.ROUTE_EARPIECE)
            CallAudio.Route.Speaker -> setAudioRoute(CallAudioState.ROUTE_SPEAKER)
            CallAudio.Route.WiredHeadset -> setAudioRoute(CallAudioState.ROUTE_WIRED_HEADSET)
            CallAudio.Route.WiredHeadsetOrEarpiece -> setAudioRoute(CallAudioState.ROUTE_WIRED_OR_EARPIECE)
        }
    }
}