/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.LocalIndication
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.material.icons.outlined.Backspace
import androidx.compose.material.icons.outlined.SimCard
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.*
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R

@Composable
fun NumberPad(
    onInput: (Char) -> Unit,
    onMailbox: (() -> Unit)?,
    lastRow: NumberpadLastRow?,
    allowPlus: Boolean
) {
    DynamicPaddingColumn(maxPadding = 48.dp) {
        Row {
            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("1"),
                subLabel = "Mailbox",
                onClick = { onInput('1') },
                onLongClick = onMailbox
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("2"),
                subLabel = "ABC",
                onClick = { onInput('2') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("3"),
                subLabel = "DEF",
                onClick = { onInput('3') },
                onLongClick = null
            )
        }

        Row {
            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("4"),
                subLabel = "GHI",
                onClick = { onInput('4') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("5"),
                subLabel = "JKL",
                onClick = { onInput('5') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("6"),
                subLabel = "MNO",
                onClick = { onInput('6') },
                onLongClick = null
            )
        }

        Row {
            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("7"),
                subLabel = "PQRS",
                onClick = { onInput('7') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("8"),
                subLabel = "TUV",
                onClick = { onInput('8') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("9"),
                subLabel = "WXYZ",
                onClick = { onInput('9') },
                onLongClick = null
            )
        }

        Row {
            NumberPadButton(
                highlight = NumberpadButtonHighlighting.None,
                label = NumberpadButtonLabel.Text("*"),
                subLabel = "",
                onClick = { onInput('*') },
                onLongClick = null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.Color,
                label = NumberpadButtonLabel.Text("0"),
                subLabel = "+",
                onClick = { onInput('0') },
                onLongClick = if (allowPlus) ({ onInput('+') }) else null
            )

            NumberPadButton(
                highlight = NumberpadButtonHighlighting.None,
                label = NumberpadButtonLabel.Text("#"),
                subLabel = "",
                onClick = { onInput('#') },
                onLongClick = null
            )
        }

        if (lastRow != null) Row {
            NumberPadButton(
                highlight = NumberpadButtonHighlighting.None,
                label = NumberpadButtonLabel.Icon(Icons.Outlined.Backspace, stringResource(R.string.dialer_delete_char)),
                subLabel = stringResource(R.string.dialer_delete_char_all),
                onClick = lastRow.delete,
                onLongClick = lastRow.deleteAll
            )

            Box(Modifier.weight(1.0f), contentAlignment = Alignment.Center) {
                FloatingActionButton(
                    onClick = lastRow.startCall,
                    shape = MaterialTheme.shapes.extraLarge,
                    containerColor = MaterialTheme.colorScheme.primary
                ) {
                    Icon(Icons.Default.Phone, stringResource(R.string.dialer_start_call))
                }
            }

            if (lastRow.phoneAccount == null) Spacer(Modifier.weight(1.0f))
            else NumberPadButton(
                highlight = NumberpadButtonHighlighting.SpecificColor(lastRow.phoneAccount.color),
                label = NumberpadButtonLabel.Icon(Icons.Outlined.SimCard, stringResource(R.string.dialer_select_sim)),
                subLabel = lastRow.phoneAccount.name,
                onClick = lastRow.phoneAccount.update,
                onLongClick = null
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun RowScope.NumberPadButton(
    highlight: NumberpadButtonHighlighting,
    label: NumberpadButtonLabel,
    subLabel: String,
    onClick: () -> Unit,
    onLongClick: (() -> Unit)?
) {
    val density = LocalDensity.current.density
    var radius by remember { mutableStateOf(16.dp) }

    val titleColor =
        when (highlight) {
            NumberpadButtonHighlighting.Color -> MaterialTheme.colorScheme.primary
            is NumberpadButtonHighlighting.SpecificColor ->
                if (highlight.color != 0) Color(highlight.color).copy(alpha = 1f)
                else LocalContentColor.current
            NumberpadButtonHighlighting.None -> LocalContentColor.current
        }

    CompositionLocalProvider(
        LocalIndication.provides(
            rememberRipple(
                bounded = false,
                radius = radius
            )
        )
    ) {
        Column (
            modifier = Modifier
                .weight(1.0f)
                .onSizeChanged { radius = (it.height.coerceAtMost(it.width) / density).dp }
                .combinedClickable(
                    enabled = true,
                    role = Role.Button,
                    onClick = onClick,
                    onClickLabel = when (label) {
                        is NumberpadButtonLabel.Text -> label.digit
                        is NumberpadButtonLabel.Icon -> label.label
                    },
                    onLongClick = onLongClick,
                    onLongClickLabel = if (onLongClick != null) subLabel else null
                )
        ) {
            val lineHeight = (MaterialTheme.typography.headlineMedium.fontSize.value * LocalDensity.current.fontScale).dp
            val lineTotalHeight = (MaterialTheme.typography.headlineMedium.lineHeight.value * LocalDensity.current.fontScale).dp
            val linePadding = lineTotalHeight - lineHeight

            when (label) {
                is NumberpadButtonLabel.Text -> Text(
                    label.digit,
                    style = MaterialTheme.typography.headlineMedium,
                    textAlign = TextAlign.Center,
                    color = titleColor,
                    modifier = Modifier.fillMaxWidth()
                )
                is NumberpadButtonLabel.Icon -> Icon(
                    imageVector = label.image,
                    contentDescription = label.label,
                    tint = titleColor,
                    modifier = Modifier
                        .align(Alignment.CenterHorizontally)
                        .padding(vertical = linePadding / 2)
                        .size(lineHeight)
                )
            }

            Text(
                subLabel,
                modifier = Modifier.align(Alignment.CenterHorizontally),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
fun DynamicPaddingColumn(
    maxPadding: Dp,
    content: @Composable () -> Unit,
) {
    val density = LocalDensity.current.density
    val maxPaddingPx = (maxPadding.value * density).toInt()

    Layout(
        content = content,
        measurePolicy = object: MeasurePolicy {
            override fun IntrinsicMeasureScope.minIntrinsicHeight(
                measurables: List<IntrinsicMeasurable>,
                width: Int
            ): Int = measurables.map { it.minIntrinsicHeight(width) }.sum()

            override fun IntrinsicMeasureScope.maxIntrinsicHeight(
                measurables: List<IntrinsicMeasurable>,
                width: Int
            ): Int =
                measurables.map { it.minIntrinsicHeight(width) }.sum() + maxPaddingPx * measurables.size

            override fun MeasureScope.measure(
                measurables: List<Measurable>,
                constraints: Constraints
            ): MeasureResult {
                val minHeight = measurables.map { it.minIntrinsicHeight(constraints.maxWidth) }.sum()
                val paddingRoom = (constraints.minHeight - minHeight).coerceAtLeast(0)
                val maxPaddingSum = maxPaddingPx * measurables.size
                val usedPaddingSum = maxPaddingSum.coerceAtMost(paddingRoom)

                return layout(constraints.maxWidth, constraints.minHeight.coerceAtLeast(minHeight)) {
                    var ySum = 0

                    measurables.map { it.measure(constraints.copy(minHeight = 0)) }.forEachIndexed { index, it ->
                        val y = ySum + usedPaddingSum * (2 * index + 1) / (2 * measurables.size)

                        it.placeRelative(0, y)

                        ySum += it.measuredHeight
                    }
                }
            }
        }
    )
}

data class NumberpadLastRow(
    val startCall: () -> Unit,
    val delete: () -> Unit,
    val deleteAll: () -> Unit,
    val phoneAccount: PhoneAccount?
) {
    data class PhoneAccount(
        val name: String,
        val color: Int,
        val update: () -> Unit
    )
}

sealed class NumberpadButtonHighlighting {
    object None: NumberpadButtonHighlighting()
    object Color: NumberpadButtonHighlighting()
    class SpecificColor(val color: Int): NumberpadButtonHighlighting()
}
sealed class NumberpadButtonLabel {
    data class Text(val digit: String): NumberpadButtonLabel()
    data class Icon(val image: ImageVector, val label: String): NumberpadButtonLabel()
}