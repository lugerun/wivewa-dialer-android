/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.about

import android.content.Context
import de.wivewa.dialer.BuildConfig
import de.wivewa.dialer.R

data class AppInfo (
    val appTitle: String,
    val appVersion: String,
    val copyrightText: String,
    val license: License,
    val libraries: List<Library>
) {
    companion object {
        fun create(context: Context) = AppInfo(
            appTitle = context.getString(R.string.app_name),
            appVersion = BuildConfig.VERSION_NAME,
            copyrightText = """
                    wivewa-dialer-android - a dialer application for Android with APIs
                    Copyright (C) 2023 Jonas Lochmann

                    This program is free software: you can redistribute it and/or modify
                    it under the terms of the GNU General Public License as published by
                    the Free Software Foundation, version 3 of the License.

                    This program is distributed in the hope that it will be useful,
                    but WITHOUT ANY WARRANTY; without even the implied warranty of
                    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
                    GNU General Public License for more details.

                    You should have received a copy of the GNU General Public License
                    along with this program. If not, see https://www.gnu.org/licenses/.
            """.trimIndent(),
            license = License.GPL_3_0,
            libraries = listOf(
                Library.ANDROIX_LIBRARIES,
                Library.KOTLIN_STANDARD_LIBRARY
            )
        )
    }
}