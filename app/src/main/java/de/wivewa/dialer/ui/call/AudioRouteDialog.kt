/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallAudio

@Composable
fun AudioRouteDialog(
    audio: CallAudio,
    onDismissRequest: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        title = { Text(stringResource(R.string.call_audio_options)) },
        text = {
            LazyColumn {
                items (audio.availableRoutes) {route ->
                    val presentation = CallAudioPresentation.forRoute(route, LocalContext.current)

                    val onClick = {
                        audio.useAudioRoute(route)
                        onDismissRequest()
                    }

                    val selected = audio.currentRoutes.contains(route)

                    Row (
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable(
                                onClickLabel = presentation.longText,
                                role = Role.RadioButton,
                                onClick = onClick
                            ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        RadioButton(selected = selected, onClick = onClick)

                        Icon(
                            presentation.icon,
                            presentation.shortText,
                            modifier = Modifier.padding(end = 8.dp)
                        )

                        Text(presentation.longText)
                    }
                }
            }
        },
        confirmButton = {
            TextButton(onClick = onDismissRequest) {
                Text(stringResource(R.string.generic_cancel))
            }
        }
    )
}