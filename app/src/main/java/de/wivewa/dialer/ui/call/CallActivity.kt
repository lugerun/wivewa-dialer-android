/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.app.KeyguardManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Bundle
import android.os.PowerManager
import android.telecom.PhoneAccount
import android.telecom.PhoneAccountHandle
import android.telecom.TelecomManager
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.content.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import de.wivewa.dialer.BuildConfig
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.dial.MainActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

class CallActivity: ComponentActivity() {
    private val model by viewModels<CallModel>()
    private var telecomManager: TelecomManager? = null

    @OptIn(FlowPreview::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        telecomManager = getSystemService()

        lifecycleScope.launch {
            model.state
                .map { it.call == null }
                .distinctUntilChanged()
                .debounce(1000)
                .collect { if (it) finish() }
        }

        lifecycleScope.launch {
            val powerManager = getSystemService<PowerManager>()!!
            val lock = powerManager.newWakeLock(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, "${BuildConfig.APPLICATION_ID}:CallActivity")

            lock.setReferenceCounted(false)

            @OptIn(ExperimentalCoroutinesApi::class)
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                model.state
                    .map { it.enableProximitySensor }
                    .distinctUntilChanged()
                    .transformLatest<_, Unit> {shouldLock ->
                        try {
                            if (shouldLock) while (true) {
                                lock.acquire(1000 * 60 * 15)

                                delay(1000 * 60 * 5)
                            }
                        } finally {
                            lock.release()
                        }
                    }
                    .collect()
            }
        }

        setContent {
            WivewaDialerTheme {
                val state by model.state.collectAsState(initial = null)

                CallScreen(
                    call = state?.call,
                    backgroundCalls = state?.backgroundCalls ?: emptyList(),
                    audio = state?.audio,
                    onAddCall = ::onAddCall,
                    onShowExternalCallerDetails = ::onShowExternalCallerDetails,
                    executeAfterKeyguardUnlock = ::afterKeyguardUnlock,
                    getPhoneAccount = ::getPhoneAccount
                )
            }
        }
    }

    private fun onAddCall() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    private fun getPhoneAccount(handle: PhoneAccountHandle): PhoneAccount = telecomManager!!.getPhoneAccount(handle)

    private fun onShowExternalCallerDetails(intent: PendingIntent) {
        afterKeyguardUnlock { intent.send() }
    }

    private fun afterKeyguardUnlock(action: () -> Unit) {
        val keyguardManager = getSystemService<KeyguardManager>()!!

        if (keyguardManager.isKeyguardLocked) {
            keyguardManager.requestDismissKeyguard(this, object: KeyguardManager.KeyguardDismissCallback() {
                override fun onDismissSucceeded() {
                    super.onDismissSucceeded()

                    action()
                }
            })
        } else action()
    }
}