/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.telecom.Call
import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CallEnd
import androidx.compose.material.icons.filled.PhoneEnabled
import androidx.compose.material.icons.filled.VolumeOff
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallInfo

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun CallBaseActions(
    call: CallInfo?
) {
    Column (
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(32.dp)
    ) {
        if (call?.state == Call.STATE_RINGING && !call.silenceRinging) {
            IconButton(onClick = call.actions.silenceRinger) {
                Icon(
                    Icons.Default.VolumeOff,
                    stringResource(R.string.ring_mute)
                )
            }
        }

        Row (
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.Center
        ) {
            val (redLabel, redAction) =
                if (call?.state == Call.STATE_RINGING)
                    Pair(stringResource(R.string.ring_reject), call.actions.reject)
                else
                    Pair(stringResource(R.string.call_disconnect), call?.actions?.disconnect ?: {})

            val greenAction = call?.actions?.answer ?: {}

            AnimatedVisibility(
                visible = call != null,
                enter = scaleIn(),
                exit = scaleOut()
            ) {
                FloatingActionButton(
                    onClick = redAction,
                    containerColor = Color.Red
                ) {
                    Icon(Icons.Default.CallEnd, redLabel)
                }
            }

            AnimatedVisibility(
                visible = call?.state == Call.STATE_RINGING,
                exit = shrinkHorizontally(
                    tween(250, 500)
                )
            ) {
                Box(modifier = Modifier.width(64.dp))
            }

            AnimatedVisibility(
                visible = call?.state == Call.STATE_RINGING,
                exit = scaleOut()
            ) {
                FloatingActionButton(
                    onClick = greenAction,
                    containerColor = Color.Green
                ) {
                    Icon(
                        Icons.Default.PhoneEnabled,
                        stringResource(R.string.ring_answer)
                    )
                }
            }
        }
    }
}