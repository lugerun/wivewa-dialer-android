/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.dialer.service.CallAudio
import de.wivewa.dialer.service.CallInfo
import de.wivewa.dialer.service.CallService
import kotlinx.coroutines.flow.*

class CallModel(application: Application): AndroidViewModel(application) {
    data class State(
        val call: CallInfo?,
        val enableProximitySensor: Boolean,
        val backgroundCalls: List<CallInfo>,
        val audio: CallAudio?
    )

    private val currentCalls = CallService.currentCalls
    private val callAudio = CallService.callAudio

    val state: Flow<State> = currentCalls.combine(callAudio) { calls, audio ->
        val ringing = CallInfo.getRinging(calls)
        val currentAndBackground = CallInfo.getCurrentAndBackground(calls)

        State(
            call = currentAndBackground?.first ?: ringing,
            enableProximitySensor = currentAndBackground?.first != null,
            backgroundCalls = currentAndBackground?.second ?: emptyList(),
            audio = audio
        )
    }.shareIn(viewModelScope, SharingStarted.WhileSubscribed(1000), 1)
}