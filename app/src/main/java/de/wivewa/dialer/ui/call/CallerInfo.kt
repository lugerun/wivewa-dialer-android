/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import android.app.PendingIntent
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.OpenInNew
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R
import de.wivewa.dialer.service.CallInfo

@Composable
fun CallerInfo (
    call: CallInfo?,
    onShowExternalCallerDetails: (PendingIntent) -> Unit
) {
    val textLines =
        if (call == null) emptyList()
        else if (call.hasChildren) listOf(stringResource(R.string.call_conference))
        else listOfNotNull(
            call.integrationDisplayName ?: call.remoteDisplayName,
            call.remotePhoneNumber,
            stringResource(R.string.call_unknown_caller)
        )

    Crossfade(targetState = textLines) { lines ->
        val firstTextLine = lines.getOrNull(0)
        val secondTextLine = lines.getOrNull(1)

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .then(
                    if (call?.integrationDetailsAction == null) Modifier
                    else Modifier.clickable(onClick = { onShowExternalCallerDetails(call.integrationDetailsAction) })
                ),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier.weight(1.0f)
            ) {
                Text(
                    firstTextLine ?: "",
                    style = MaterialTheme.typography.headlineLarge,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )

                Text(
                    secondTextLine ?: "",
                    style = MaterialTheme.typography.headlineMedium,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.fillMaxWidth()
                )
            }

            if (call?.integrationDetailsAction != null) {
                Icon(
                    Icons.Default.OpenInNew,
                    stringResource(R.string.call_details),
                    modifier = Modifier.padding(8.dp)
                )
            }
        }
    }
}