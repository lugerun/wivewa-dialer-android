/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.call

import androidx.activity.compose.BackHandler
import androidx.compose.animation.*
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.surfaceColorAtElevation
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.ui.NumberPad

@Composable
fun InCallNumberPad(
    visible: Boolean,
    playDtmfTone: (Char) -> Unit,
    onDismissRequest: () -> Unit
) {
    if (visible) {
        BackHandler(onBack = onDismissRequest)
    }

    AnimatedVisibility(
        visible = visible,
        enter = fadeIn(),
        exit = fadeOut()
    ) {
        Box(
            modifier = Modifier
                .background(MaterialTheme.colorScheme.scrim.copy(alpha = 0.25f))
                .fillMaxSize()
        )
    }

    AnimatedVisibility(
        visible = visible,
        enter = slideInVertically { it },
        exit = slideOutVertically { it }
    ) {
        Layout (
            modifier = Modifier
                .clickable(onClick = onDismissRequest)
                .fillMaxSize()
                .verticalScroll(rememberScrollState()),
            content = {
                Surface(
                    color = MaterialTheme.colorScheme.surfaceColorAtElevation(8.dp),
                    contentColor = MaterialTheme.colorScheme.onSurface
                ) {
                    NumberPad(
                        onInput = playDtmfTone,
                        allowPlus = false,
                        onMailbox = null,
                        lastRow = null
                    )
                }
            }
        ) { measurables, constraints ->
            val view = measurables.single()

            val width = constraints.maxWidth

            val minHeight = view.minIntrinsicHeight(width)
            val maxHeight = view.maxIntrinsicHeight(width)

            val effectiveHeight = maxHeight.coerceAtMost(
                constraints.minHeight.coerceAtLeast(minHeight)
            )

            val measured = view.measure(
                constraints.copy(
                    minHeight = effectiveHeight,
                    maxHeight = effectiveHeight
                )
            )

            val resultHeight = measured.measuredHeight.coerceAtLeast(constraints.minHeight)

            layout(width, resultHeight) {
                val y = resultHeight - measured.measuredHeight

                measured.place(0, y)
            }
        }
    }
}