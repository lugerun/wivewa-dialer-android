/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ClearAll
import androidx.compose.material3.*
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.core.content.getSystemService
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.compose.collectAsLazyPagingItems
import de.wivewa.dialer.R
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.Common
import kotlinx.coroutines.launch

class CallLogActivity: ComponentActivity() {
    private val model by viewModels<CallLogModel>()

    private val requestPermissions = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { model.recheckPermission() }

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                for (command in model.activityCommand) {
                    when (command) {
                        is CallLogModel.ActivityCommand.RequestPermission -> requestPermissions.launch(model.requiredPermissions)
                    }
                }
            }
        }

        val clipboardManager = getSystemService<ClipboardManager>()!!
        val actions = CallLogItemActions(
            copyNumberToClipboard = {
                clipboardManager.setPrimaryClip(
                    ClipData.newPlainText(
                        getString(R.string.call_log_action_clipboard_data_type),
                        it
                    )
                )
            },
            dialNumber = {
                startActivity(
                    Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", it, null))
                        .addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        .setPackage(packageName)
                )
            }
        )

        setContent {
            WivewaDialerTheme {
                val status by model.status.collectAsState(initial = null)

                var showClearDialog by rememberSaveable { mutableStateOf(false) }

                Scaffold(
                    topBar = {
                        TopAppBar(
                            title = {
                                Text(stringResource(R.string.call_log_title))
                            },
                            actions = {
                                AnimatedVisibility(visible = status is CallLogModel.Status.Ready) {
                                    IconButton(onClick = { showClearDialog = true }) {
                                        Icon(
                                            Icons.Default.ClearAll,
                                            stringResource(R.string.call_log_clear_title)
                                        )
                                    }
                                }
                            },
                            colors = Common.topAppBarColors()
                        )
                    }
                ) { padding ->
                    val nowLive by model.now.collectAsState(initial = null)
                    val list = model.pager.collectAsLazyPagingItems()
                    val expendedCallId by model.expandedCallId.collectAsState(initial = null)

                    val modifier = Modifier.padding(padding)
                    val now = nowLive

                    if (now != null) when (val screen = status) {
                        is CallLogModel.Status.MissingPermission -> MissingCallLogPermission(
                            requestPermission = screen.requestPermission,
                            modifier = modifier
                        )
                        CallLogModel.Status.Empty -> EmptyCallLog(modifier = modifier)
                        is CallLogModel.Status.Ready -> CallLogList(
                            modifier = modifier,
                            listState = screen.listState,
                            list = list,
                            now = now,
                            expandedCallId = expendedCallId,
                            updateExpandedCallId = { model.expandedCallId.value = it },
                            actions = actions
                        )
                        null -> {/* nothing to do */}
                    }
                }

                if (showClearDialog) ClearCallLogDialog(
                    onDismissRequest = { showClearDialog = false },
                    onTriggerDeletion = { model.wipeData() }
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()

        model.recheckPermission()
        model.reportCallsRead()
    }
}