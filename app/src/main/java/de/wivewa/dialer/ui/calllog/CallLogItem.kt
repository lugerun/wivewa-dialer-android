/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

data class CallLogItem (
    val id: Long,
    val time: Long,
    val type: Type?,
    val duration: Long,
    val number: String,
    val contactName: String?
) {
    enum class Type {
        Incoming,
        Missed,
        Outgoing,
        Mailbox,
        Blocked,
        ExternallyAnswered,
        Rejected
    }
}