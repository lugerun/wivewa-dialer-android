/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import de.wivewa.dialer.R
import android.text.format.DateUtils
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.items

@Composable
fun CallLogList(
    modifier: Modifier,
    list: LazyPagingItems<CallLogItem>,
    listState: LazyListState,
    expandedCallId: Long?,
    updateExpandedCallId: (Long?) -> Unit,
    actions: CallLogItemActions,
    now: Long
) {
    LazyColumn (
        verticalArrangement = Arrangement.spacedBy(8.dp),
        contentPadding = PaddingValues(8.dp),
        modifier = modifier,
        state = listState
    ) {
        items(list, key = { it.id }) {
            CallLogListItem(
                item = it,
                now = now,
                isExpanded = it?.id == expandedCallId,
                onUpdateExpanded = { expand ->
                    if (expand) updateExpandedCallId(it?.id)
                    else updateExpandedCallId(null)
                },
                actions = actions
            )
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun LazyItemScope.CallLogListItem(
    item: CallLogItem?,
    now: Long,
    isExpanded: Boolean,
    onUpdateExpanded: (Boolean) -> Unit,
    actions: CallLogItemActions
) {
    val placeholderColor = MaterialTheme.colorScheme.scrim.copy(alpha = 0.5f)

    Card (
        modifier = Modifier
            .clickable { onUpdateExpanded(!isExpanded) }
            .fillMaxWidth()
            .animateItemPlacement()
    ) {
        Column (
            modifier = Modifier.padding(8.dp),
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    modifier = Modifier.weight(1.0f)
                ) {
                    if (item == null) {
                        Text(
                            "123456789",
                            color = placeholderColor,
                            modifier = Modifier.background(placeholderColor)
                        )
                    } else if (item.contactName != null) {
                        Text(
                            item.contactName,
                            style = MaterialTheme.typography.titleLarge
                        )

                        Text(item.number)
                    } else {
                        Text(
                            item.number,
                            style = MaterialTheme.typography.titleLarge
                        )
                    }

                    if (item == null) {
                        Text(
                            "a few minutes ago - 12 minutes",
                            color = placeholderColor,
                            modifier = Modifier.background(placeholderColor)
                        )
                    } else {
                        val startTime =
                            DateUtils.getRelativeTimeSpanString(
                                item.time,
                                now,
                                DateUtils.MINUTE_IN_MILLIS,
                                DateUtils.FORMAT_SHOW_DATE
                            )

                        val duration =
                            if (item.duration > 0) DateUtils.formatElapsedTime(item.duration)
                            else null

                        val parts = listOfNotNull(startTime, duration).joinToString(separator = " - ")

                        Text(parts)
                    }
                }

                if (item?.type != null)
                    Icon(
                        when (item.type) {
                            CallLogItem.Type.Incoming -> Icons.Default.CallReceived
                            CallLogItem.Type.Missed -> Icons.Default.CallMissed
                            CallLogItem.Type.Outgoing -> Icons.Default.CallMade
                            CallLogItem.Type.Mailbox -> Icons.Default.Voicemail
                            CallLogItem.Type.Blocked -> Icons.Default.Block
                            CallLogItem.Type.ExternallyAnswered -> Icons.Default.PhoneForwarded
                            CallLogItem.Type.Rejected -> Icons.Default.CallEnd
                        },
                        contentDescription = when (item.type) {
                            CallLogItem.Type.Incoming -> stringResource(R.string.call_log_type_incoming)
                            CallLogItem.Type.Missed -> stringResource(R.string.call_log_type_missed)
                            CallLogItem.Type.Outgoing -> stringResource(R.string.call_log_type_outgoing)
                            CallLogItem.Type.Mailbox -> stringResource(R.string.call_log_type_mailbox)
                            CallLogItem.Type.Blocked -> stringResource(R.string.call_log_type_block)
                            CallLogItem.Type.ExternallyAnswered -> stringResource(R.string.call_log_type_externally_answered)
                            CallLogItem.Type.Rejected -> stringResource(R.string.call_log_type_rejected)
                        },
                        modifier = Modifier
                            .size(40.dp)
                            .padding(end = 8.dp)
                    )
            }

            AnimatedVisibility(visible = isExpanded) {
                Column (
                    modifier = Modifier
                        .padding(top = 4.dp)
                        .fillMaxWidth()
                ) {
                    Text(DateUtils.formatDateTime(
                        LocalContext.current,
                        item?.time ?: 0,
                        DateUtils.FORMAT_SHOW_DATE or
                                DateUtils.FORMAT_SHOW_TIME or
                                DateUtils.FORMAT_SHOW_WEEKDAY
                    ))

                    if (item != null) {
                        Row(
                            horizontalArrangement = Arrangement.End,
                            modifier = Modifier.fillMaxWidth()
                        ) {
                            TextButton(onClick = { actions.dialNumber(item.number) }) {
                                Text(stringResource(R.string.call_log_action_call))
                            }

                            TextButton(onClick = { actions.copyNumberToClipboard(item.number) }) {
                                Text(stringResource(R.string.call_log_action_clipboard))
                            }
                        }
                    }
                }
            }
        }
    }
}

class CallLogItemActions(
    val copyNumberToClipboard: (String) -> Unit,
    val dialNumber: (String) -> Unit
)