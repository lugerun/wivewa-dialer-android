/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.calllog

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.provider.CallLog
import androidx.core.content.ContextCompat
import androidx.paging.PagingSource
import androidx.paging.PagingState
import de.wivewa.dialer.data.SystemContacts
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.invoke

class CallLogPagingSource(private val context: Context): PagingSource<CallLogPagingSource.Key, CallLogItem>() {
    sealed class Key {
        class AfterItem(val id: Long, val time: Long): Key()
    }

    override fun getRefreshKey(state: PagingState<Key, CallLogItem>): Key? =
        state.anchorPosition?.let { state.closestPageToPosition(it) }?.prevKey

    override suspend fun load(params: LoadParams<Key>): LoadResult<Key, CallLogItem> = Dispatchers.IO.invoke {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED)
            return@invoke LoadResult.Error(SecurityException())

        val key = params.key

        val uri = CallLog.Calls.CONTENT_URI.buildUpon()
            .appendQueryParameter(CallLog.Calls.LIMIT_PARAM_KEY, params.loadSize.toString())
            .build()

        val projection = arrayOf(
            CallLog.Calls._ID,
            CallLog.Calls.DATE,
            CallLog.Calls.TYPE,
            CallLog.Calls.DURATION,
            CallLog.Calls.NUMBER
        )

        val (selection, selectionArgs) =
            when (key) {
                is Key.AfterItem -> Pair(
                    "${CallLog.Calls.DATE} = ? AND ${CallLog.Calls._ID} > ? OR ${CallLog.Calls.DATE} < ?",
                    arrayOf(key.time.toString(), key.id.toString(), key.time.toString())
                )
                null -> Pair(null, null)
            }

        val sortOrder = "${CallLog.Calls.DATE} DESC, ${CallLog.Calls._ID} ASC"

        context.contentResolver.query(
            uri,
            projection,
            selection,
            selectionArgs,
            sortOrder
        )?.use { cursor ->
            val idColumn = cursor.getColumnIndexOrThrow(CallLog.Calls._ID)
            val dateColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.DATE)
            val typeColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.TYPE)
            val durationColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.DURATION)
            val numberColumn = cursor.getColumnIndexOrThrow(CallLog.Calls.NUMBER)

            mutableListOf<CallLogItem>().also { list ->
                if (cursor.moveToFirst()) do {
                    val type = when (cursor.getInt(typeColumn)) {
                        CallLog.Calls.INCOMING_TYPE -> CallLogItem.Type.Incoming
                        CallLog.Calls.MISSED_TYPE -> CallLogItem.Type.Missed
                        CallLog.Calls.OUTGOING_TYPE -> CallLogItem.Type.Outgoing
                        CallLog.Calls.VOICEMAIL_TYPE -> CallLogItem.Type.Mailbox
                        CallLog.Calls.BLOCKED_TYPE -> CallLogItem.Type.Blocked
                        CallLog.Calls.ANSWERED_EXTERNALLY_TYPE -> CallLogItem.Type.ExternallyAnswered
                        CallLog.Calls.REJECTED_TYPE -> CallLogItem.Type.Rejected
                        else -> null
                    }

                    list.add(CallLogItem(
                        id = cursor.getLong(idColumn),
                        time = cursor.getLong(dateColumn),
                        type = type,
                        duration = cursor.getLong(durationColumn),
                        number = cursor.getString(numberColumn),
                        contactName = null
                    ))
                } while (cursor.moveToNext())
            }.toList()
        }?.let { items ->
            val contacts = SystemContacts.query(context, items.map { it.number })

            items.map { item ->
                item.copy(contactName = contacts[item.number]?.displayName)
            }
        }?.let { items ->
            val nextKey = items.lastOrNull()?.let {
                Key.AfterItem(id = it.id, time = it.time)
            }

            return@invoke LoadResult.Page(items, null, nextKey)
        }

        LoadResult.Error(NullPointerException())
    }
}