/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.ui.NumberPad
import de.wivewa.dialer.ui.NumberpadLastRow

@OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
@Composable
fun DialScreen(screen: MainModel.State.Dialpad) {
    val value = screen.value

    val inputDelete = {
        val textBefore = value.text.substring(
            0,
            value.selection.start
                .coerceAtLeast(0)
                .coerceAtMost(value.text.length)
        )

        val textAfter = value.text.substring(
            value.selection.end.coerceAtMost(
                value.text.length
            )
        )

        val newTextBeforeLength = (textBefore.length - 1).coerceAtLeast(0)

        screen.updateValue(
            value.copy(
                text = "${textBefore.substring(0, newTextBeforeLength)}$textAfter",
                selection = TextRange(newTextBeforeLength)
            )
        )
    }

    ExpandFirstViewColumn (
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(rememberScrollState())
    ) {
        Box(
            contentAlignment = Alignment.CenterStart
        ) {
            Text(
                screen.contactName ?: "",
                textAlign = TextAlign.Center,
                style = MaterialTheme.typography.headlineMedium,
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
            )
        }

        Surface (
            color = MaterialTheme.colorScheme.surfaceColorAtElevation(8.dp)
        ) {
            MinHeightToSecondViewColumn (
                Modifier.animateContentSize()
            ) {
                TextField(
                    value = value,
                    onValueChange = screen.updateValue,
                    modifier = Modifier.fillMaxWidth(),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Phone,
                        imeAction = ImeAction.Go
                    ),
                    keyboardActions = KeyboardActions(
                        onGo = { screen.startDial() }
                    ),
                    singleLine = true,
                    colors = TextFieldDefaults.textFieldColors(
                        containerColor = Color.Unspecified,
                        focusedIndicatorColor = Color.Unspecified,
                        unfocusedIndicatorColor = Color.Unspecified
                    ),
                    textStyle = MaterialTheme.typography.headlineMedium.copy(textAlign = TextAlign.Center)
                )

                if (!WindowInsets.isImeVisible) NumberPad(
                    allowPlus = true,
                    onInput = {
                        val textBefore = value.text.substring(
                            0,
                            value.selection.start
                                .coerceAtLeast(0)
                                .coerceAtMost(value.text.length)
                        )

                        val textAfter = value.text.substring(
                            value.selection.end.coerceAtMost(
                                value.text.length
                            )
                        )

                        screen.updateValue(
                            value.copy(
                                text = "$textBefore$it$textAfter",
                                selection = TextRange(textBefore.length + 1)
                            )
                        )
                    },
                    onMailbox = screen.callMailbox,
                    lastRow = NumberpadLastRow(
                        startCall = screen.startDial,
                        delete = inputDelete,
                        deleteAll = { screen.updateValue(TextFieldValue()) },
                        phoneAccount = screen.currentPhoneAccount?.let { phoneAccount ->
                            NumberpadLastRow.PhoneAccount(
                                phoneAccount.label.toString(),
                                phoneAccount.highlightColor,
                                screen.openPhoneAccountDialog
                            )
                        }
                    )
                ) else Spacer(Modifier.height((
                        (WindowInsets.ime.getBottom(LocalDensity.current) -
                                WindowInsets.navigationBars.getBottom(LocalDensity.current)) /
                                LocalDensity.current.density).dp
                ))
            }
        }
    }

    if (screen.phoneAccountDialog != null) PhoneAccountDialog(
        currentPhoneAccount = screen.currentPhoneAccount,
        phoneAccounts = screen.phoneAccountDialog,
        selectAccount = screen.selectPhoneAccount,
        onDismissRequest = screen.closePhoneAccountDialog
    )
}