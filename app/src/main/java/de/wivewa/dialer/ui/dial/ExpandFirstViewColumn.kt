/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.Layout

@Composable
fun ExpandFirstViewColumn(
    modifier: Modifier = Modifier,
    content: @Composable () -> Unit
) {
    Layout(
        modifier = modifier,
        content = content
    ) { measurables, constraints ->
        if (measurables.size != 2) throw IllegalStateException()

        val header = measurables[0]
        val body = measurables[1]

        val resultMinHeight = constraints.minHeight
        val headerMinHeight = header.minIntrinsicHeight(constraints.maxWidth)
        val bodyMinHeight = body.minIntrinsicHeight(constraints.maxWidth)
        val bodyMaxHeight = body.maxIntrinsicHeight(constraints.maxWidth)

        val takenBodyHeight = (resultMinHeight - headerMinHeight)
            .coerceAtLeast(bodyMinHeight)
            .coerceAtMost(bodyMaxHeight)

        val takenHeaderHeight = (resultMinHeight - takenBodyHeight)
            .coerceAtLeast(headerMinHeight)

        val measuredHeader = header.measure(
            constraints.copy(minHeight = takenHeaderHeight)
        )

        val measuredBody = body.measure(
            constraints.copy(minHeight = takenBodyHeight)
        )

        layout(
            constraints.maxWidth,
            measuredHeader.measuredHeight + measuredBody.measuredHeight
        ) {
            measuredHeader.placeRelative(0, 0)
            measuredBody.placeRelative(0, measuredHeader.measuredHeight)
        }
    }
}