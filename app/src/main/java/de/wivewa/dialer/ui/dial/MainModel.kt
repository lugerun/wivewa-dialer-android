/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import android.annotation.SuppressLint
import android.app.Application
import android.app.role.RoleManager
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.telecom.PhoneAccount
import android.telecom.PhoneAccountHandle
import android.telecom.TelecomManager
import android.widget.Toast
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.TextFieldValue
import androidx.core.content.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.dialer.R
import de.wivewa.dialer.data.PhoneAccounts
import de.wivewa.dialer.data.SystemContacts
import de.wivewa.dialer.extensions.placeCallAndWait
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

@OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
class MainModel(application: Application): AndroidViewModel(application) {
    sealed class State {
        data class MissingRole(
            val request: () -> Unit
        ): State()

        data class Dialpad(
            val value: TextFieldValue,
            val contactName: String?,
            val updateValue: (TextFieldValue) -> Unit,
            val startDial: () -> Unit,
            val callMailbox: () -> Unit,
            val phoneAccountDialog: List<PhoneAccount>?,
            val currentPhoneAccount: PhoneAccount?,
            val openPhoneAccountDialog: () -> Unit,
            val closePhoneAccountDialog: () -> Unit,
            val selectPhoneAccount: (PhoneAccount) -> Unit
        ): State()
    }

    sealed class ActivityCommand {
        object RequestRole: ActivityCommand()
        object LaunchCallScreen: ActivityCommand()
        object LaunchContacts: ActivityCommand()
    }

    private val roleManager = application.getSystemService<RoleManager>()!!
    private val telecomManager = application.getSystemService<TelecomManager>()!!

    private val valueLive = MutableStateFlow(TextFieldValue())
    private val valueContact: Flow<SystemContacts.Item?> = valueLive
        .debounce(250)
        .mapLatest { SystemContacts.query(application, it.text) }
        .distinctUntilChanged()

    private val activityCommandChannelInternal = Channel<ActivityCommand>()
    val activityCommand: ReceiveChannel<ActivityCommand> = activityCommandChannelInternal

    private val retryHasRole = MutableStateFlow(0)
    private val hasRole = retryHasRole.map { roleManager.isRoleHeld(RoleManager.ROLE_DIALER) }
    fun retryHasRole() { retryHasRole.update { it + 1 } }
    val requestRoleIntent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER)

    private val currentPhoneAccountLive = MutableStateFlow<PhoneAccountHandle?>(null)
    private val showPhoneAccountDialogLive = MutableStateFlow(false)

    val status: Flow<State> = hasRole
        .distinctUntilChanged()
        .transformLatest @SuppressLint("MissingPermission") { hasRole ->
            if (hasRole) {
                val phoneAccountsLive = PhoneAccounts.get(getApplication(), telecomManager)

                emitAll(
                    combine(
                        valueLive, valueContact, phoneAccountsLive, currentPhoneAccountLive, showPhoneAccountDialogLive
                    ) { value, contact, phoneAccounts, currentPhoneAccount, showPhoneAccountDialog ->
                        val effectivePhoneAccount =
                            currentPhoneAccount.let { handle ->
                                phoneAccounts.find { it.accountHandle == currentPhoneAccount }
                            } ?: try {
                                telecomManager.getPhoneAccount(
                                    telecomManager.getDefaultOutgoingPhoneAccount("tel")
                                )
                            } catch (ex: Exception) {
                                null
                            } ?: phoneAccounts.firstOrNull()

                        val otherPhoneAccount = (phoneAccounts - effectivePhoneAccount).singleOrNull()

                        if (currentPhoneAccount != effectivePhoneAccount?.accountHandle)
                            currentPhoneAccountLive.compareAndSet(currentPhoneAccount, effectivePhoneAccount?.accountHandle)

                        State.Dialpad(
                            value = value,
                            contactName = contact?.displayName,
                            updateValue = { newValue -> valueLive.update { newValue } },
                            startDial = {
                                val text = value.text

                                if (text.isNotBlank()) startDial(value.text)
                                else viewModelScope.launch { activityCommandChannelInternal.send(ActivityCommand.LaunchContacts) }
                            },
                            callMailbox = {
                                try {
                                    startDial(telecomManager.getVoiceMailNumber(currentPhoneAccount!!))
                                } catch (ex: Exception) {
                                    Toast.makeText(getApplication(), R.string.dialer_start_call_failure, Toast.LENGTH_SHORT).show()
                                }
                            },
                            phoneAccountDialog = if (showPhoneAccountDialog) phoneAccounts else null,
                            currentPhoneAccount = effectivePhoneAccount,
                            openPhoneAccountDialog = {
                                if (otherPhoneAccount == null) showPhoneAccountDialogLive.value = true
                                else currentPhoneAccountLive.value = otherPhoneAccount.accountHandle
                            },
                            closePhoneAccountDialog = { showPhoneAccountDialogLive.value = false },
                            selectPhoneAccount = {
                                currentPhoneAccountLive.value = it.accountHandle
                                showPhoneAccountDialogLive.value = false
                            }
                        )
                    }
                )
            }
            else emit(State.MissingRole { activityCommandChannelInternal.trySend(ActivityCommand.RequestRole) })
        }.shareIn(viewModelScope, SharingStarted.WhileSubscribed(1000), 1)

    fun handleIntent(intent: Intent) {
        if (intent.action == Intent.ACTION_DIAL || intent.action == Intent.ACTION_VIEW) {
            val data = intent.data ?: return
            if (data.scheme != "tel") return

            valueLive.value = TextFieldValue(
                data.schemeSpecificPart,
                TextRange(data.schemeSpecificPart.length)
            )
        }
    }

    private fun startDial(number: String) {
        viewModelScope.launch {
            try {
                val uri = Uri.fromParts("tel", number, null)

                val extras = Bundle().also {
                    currentPhoneAccountLive.value?.let { handle ->
                        it.putParcelable(TelecomManager.EXTRA_PHONE_ACCOUNT_HANDLE, handle)
                    }
                }

                if (telecomManager.placeCallAndWait(uri, extras, null)) {
                    valueLive.value = TextFieldValue()
                    currentPhoneAccountLive.value = null
                    activityCommandChannelInternal.send(ActivityCommand.LaunchCallScreen)
                }
            } catch (ex: SecurityException) {
                Toast.makeText(getApplication(), R.string.dialer_start_call_failure, Toast.LENGTH_SHORT).show()
            }
        }
    }
}