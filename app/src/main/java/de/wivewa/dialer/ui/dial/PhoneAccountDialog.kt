/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.dial

import android.telecom.PhoneAccount
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.Role
import de.wivewa.dialer.R

@Composable
fun PhoneAccountDialog(
    currentPhoneAccount: PhoneAccount?,
    phoneAccounts: List<PhoneAccount>,
    selectAccount: (PhoneAccount) -> Unit,
    onDismissRequest: () -> Unit
) {
    AlertDialog(
        onDismissRequest = onDismissRequest,
        title = { Text(stringResource(R.string.dialer_select_sim)) },
        text = {
            LazyColumn {
                items (phoneAccounts) {account ->
                    val selected = account == currentPhoneAccount
                    val label = account.label.toString()
                    val onClick = { selectAccount(account) }

                    Row (
                        modifier = Modifier
                            .fillMaxWidth()
                            .clickable(
                                onClickLabel = label,
                                role = Role.RadioButton,
                                onClick = onClick
                            ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        RadioButton(selected = selected, onClick = onClick)

                        Text(label)
                    }
                }
            }
        },
        confirmButton = {
            TextButton(onClick = onDismissRequest) {
                Text(stringResource(R.string.generic_cancel))
            }
        }
    )
}