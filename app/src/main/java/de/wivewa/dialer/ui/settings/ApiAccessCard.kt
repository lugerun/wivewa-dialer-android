/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.settings

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TriStateCheckbox
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.state.ToggleableState
import androidx.compose.ui.unit.dp
import de.wivewa.dialer.R
import de.wivewa.android.integration.dialer.server.configuration.ClientApp
import de.wivewa.android.integration.dialer.server.configuration.ClientAppWithState

@Composable
fun ApiAccessCard(
    apps: List<ClientAppWithState>,
    grantAccess: (ClientApp) -> Unit,
    revokeAccess: (ClientApp) -> Unit
) {
    Card (
        modifier = Modifier.fillMaxWidth()
    ) {
        Column(
            modifier = Modifier.padding(8.dp),
            verticalArrangement = Arrangement.spacedBy(4.dp)
        ) {
            Text(
                stringResource(R.string.settings_api_title),
                style = MaterialTheme.typography.headlineMedium
            )

            Text(stringResource(R.string.settings_api_text))

            for (app in apps) {
                val onClick: () -> Unit = {
                    if (app.state is ClientAppWithState.State.Forbidden) grantAccess(app.app)
                    else revokeAccess(app.app)
                }

                Row(
                    modifier = Modifier
                        .clickable(onClick = onClick)
                        .fillMaxWidth(),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    TriStateCheckbox(
                        state = when (app.state) {
                            ClientAppWithState.State.Forbidden -> ToggleableState.Off
                            ClientAppWithState.State.Allowed -> ToggleableState.On
                            is ClientAppWithState.State.Error -> ToggleableState.Indeterminate
                        },
                        onClick = onClick
                    )

                    Column() {
                        Text(
                            app.app.title,
                            style = MaterialTheme.typography.headlineSmall
                        )

                        Text(app.app.packageName)
                    }
                }
            }

            if (apps.isEmpty()) {
                Text(stringResource(R.string.settings_api_empty))
            }
        }
    }
}