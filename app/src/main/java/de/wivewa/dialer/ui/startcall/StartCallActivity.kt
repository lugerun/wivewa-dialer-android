/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.startcall

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.material3.ShapeDefaults
import androidx.compose.material3.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.window.Dialog
import androidx.lifecycle.lifecycleScope
import de.wivewa.dialer.theme.WivewaDialerTheme
import de.wivewa.dialer.ui.call.CallActivity
import de.wivewa.dialer.ui.dial.MainActivity
import de.wivewa.dialer.ui.dial.RoleScreen
import kotlinx.coroutines.launch

class StartCallActivity: ComponentActivity() {
    private val model by viewModels<StartCallModel>()

    private val requestRole = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            model.retryHasRole()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.startWith(intent)

        lifecycleScope.launch {
            lifecycleScope.launchWhenResumed {
                for (command in model.activityCommand) {
                    when (command) {
                        StartCallModel.ActivityCommand.RequestRole -> requestRole.launch(model.requestRoleIntent)
                        StartCallModel.ActivityCommand.SwitchToCallScreen -> {
                            startActivity(Intent(this@StartCallActivity, CallActivity::class.java))

                            finish()
                        }
                        StartCallModel.ActivityCommand.SwitchToDialScreen -> {
                            startActivity(Intent(this@StartCallActivity, MainActivity::class.java))

                            finish()
                        }
                        StartCallModel.ActivityCommand.Finish -> finish()
                    }
                }
            }
        }

        setContent {
            WivewaDialerTheme {
                val status by model.status.collectAsState(initial = null)

                when (val screen = status) {
                    is StartCallModel.Status.MissingRole -> Dialog(onDismissRequest = { finish() }) {
                        Surface(shape = ShapeDefaults.Medium) {
                            RoleScreen(requestRole = screen.request)
                        }
                    }
                    null -> {/* nothing to show */}
                }
            }
        }
    }
}