/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.dialer.ui.startcall

import android.app.Application
import android.app.role.RoleManager
import android.content.Intent
import android.telecom.TelecomManager
import android.widget.Toast
import androidx.core.content.getSystemService
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import de.wivewa.dialer.R
import de.wivewa.dialer.extensions.placeCallAndWait
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean

class StartCallModel(application: Application): AndroidViewModel(application) {
    sealed class ActivityCommand {
        object RequestRole: ActivityCommand()
        object SwitchToCallScreen: ActivityCommand()
        object SwitchToDialScreen: ActivityCommand()
        object Finish: ActivityCommand()
    }

    sealed class Status {
        data class MissingRole(
            val request: () -> Unit
        ): Status()
    }

    private val roleManager = application.getSystemService<RoleManager>()!!
    private val telecomManager = application.getSystemService<TelecomManager>()!!

    private val didStart = AtomicBoolean(false)
    private val activityCommandChannelInternal = Channel<ActivityCommand>()
    private val statusInternal = MutableStateFlow(null as Status?)
    private val retryHasRole = Channel<Unit>()

    val activityCommand: ReceiveChannel<ActivityCommand> = activityCommandChannelInternal
    val status: Flow<Status?> = statusInternal

    val requestRoleIntent = roleManager.createRequestRoleIntent(RoleManager.ROLE_DIALER)

    fun startWith(intent: Intent) {
        if (didStart.compareAndSet(false, true)) viewModelScope.launch {
            try {
                val data = intent.data
                val extras = intent.getBundleExtra(TelecomManager.EXTRA_OUTGOING_CALL_EXTRAS)

                if (intent.action == Intent.ACTION_CALL && data != null) {
                    while (!roleManager.isRoleHeld(RoleManager.ROLE_DIALER)) {
                        statusInternal.update {
                            Status.MissingRole {
                                activityCommandChannelInternal.trySend(ActivityCommand.RequestRole)
                            }
                        }

                        retryHasRole.receive()
                    }

                    statusInternal.update { null }

                    if (telecomManager.placeCallAndWait(data, null, extras)) {
                        activityCommandChannelInternal.send(ActivityCommand.SwitchToCallScreen)
                    }
                } else {
                    activityCommandChannelInternal.send(ActivityCommand.SwitchToDialScreen)
                }
            } catch (ex: SecurityException) {
                Toast.makeText(getApplication(), R.string.dialer_start_call_failure, Toast.LENGTH_SHORT).show()
            } finally {
                activityCommandChannelInternal.send(ActivityCommand.Finish)
            }
        }
    }

    fun retryHasRole() { retryHasRole.trySend(Unit) }
}