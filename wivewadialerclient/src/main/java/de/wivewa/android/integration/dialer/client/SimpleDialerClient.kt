/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.client

import android.content.*
import android.os.DeadObjectException
import android.os.Handler
import android.os.IBinder
import android.telecom.TelecomManager
import de.wivewa.android.integration.dialer.CallExtras
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.DialerIntegrationActions
import de.wivewa.android.integration.dialer.WivewaDialerServer
import java.util.concurrent.Executors

class SimpleDialerClient (private val context: Context, private val handler: Handler) {
    internal sealed class State {
        object Stopped: State()
        object Disconnected: State()
        class Connecting(val listeners: MutableList<(Result<WivewaDialerServer>) -> Unit>): State()
        class Connected(val api: WivewaDialerServer): State()
    }

    class StoppedException: RuntimeException()
    class UnsupportedDialerException: RuntimeException()

    private val telecomManager by lazy { context.getSystemService(Context.TELECOM_SERVICE) as TelecomManager }
    private val executor = Executors.newSingleThreadExecutor()
    private var state: State = State.Stopped

    private val connection = object: ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, binder: IBinder) {
            val server = WivewaDialerServer.Stub.asInterface(binder)

            synchronized(this@SimpleDialerClient) {
                val stateCopy = state

                if (stateCopy is State.Connecting) {
                    state = State.Connected(server)

                    stateCopy.listeners
                } else emptyList()
            }.also { list ->
                val result = Result.success(server)

                list.forEach { handler.post { it(result) } }
            }
        }

        override fun onNullBinding(name: ComponentName?) {
            super.onNullBinding(name)

            unbind()
        }

        override fun onBindingDied(name: ComponentName?) {
            super.onBindingDied(name)

            unbind()
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            synchronized(this@SimpleDialerClient) {
                val stateCopy = state

                if (stateCopy is State.Connected) {
                    state = State.Connecting(mutableListOf())
                }
            }
        }
    }

    private val dialerChangeListener = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            unbind()
        }
    }

    fun start() {
        synchronized(this) {
            if (state == State.Stopped) {
                state = State.Disconnected

                context.registerReceiver(dialerChangeListener, IntentFilter(TelecomManager.ACTION_DEFAULT_DIALER_CHANGED))
            }
        }
    }

    fun stop() {
        synchronized(this) {
            unbind()

            if (state == State.Disconnected) {
                state = State.Stopped

                context.unregisterReceiver(dialerChangeListener)
            }
        }
    }

    fun checkPermissionGranted(listener: (Result<Boolean>) -> Unit) {
        bindAnd {
            it.mapCatching { it.wasPermissionGranted() }.also { result ->
                handler.post { listener(result) }
            }
        }
    }

    fun requestPermissionIntent(): Intent = Intent(DialerIntegrationActions.ACTION_REQUEST_PERMISSION)
        .setPackage(telecomManager.defaultDialerPackage)

    fun getCurrentCalls(listener: (Result<List<CallInfo>>) -> Unit) {
        bindAnd {
            it.mapCatching { it.currentCalls }.also { result ->
                handler.post { listener(result) }
            }
        }
    }

    fun enhanceCall(callId: String, extras: CallExtras, listener: (Result<Unit>) -> Unit) {
        bindAnd {
            it.mapCatching { it.enhanceCall(callId, extras) }.also { result ->
                handler.post { listener(result) }
            }
        }
    }

    private fun bindAnd(action: (Result<WivewaDialerServer>) -> Unit) {
        synchronized(this) {
            val stateBackup = state

            when (stateBackup) {
                is State.Stopped -> executor.submit { action(Result.failure(StoppedException())) }
                is State.Connected -> executor.submit { action(Result.success(stateBackup.api)) }
                is State.Connecting -> stateBackup.listeners.add(action)
                is State.Disconnected -> {
                    val dialerPackage = telecomManager.defaultDialerPackage

                    if (dialerPackage == null) {
                        executor.submit { action(Result.failure(UnsupportedDialerException())) }
                    } else {
                        val intent = Intent(DialerIntegrationActions.ACTION_DIALER_SERVICE)
                            .setPackage(dialerPackage)

                        try {
                            if (!context.bindService(intent, connection, Context.BIND_AUTO_CREATE)) {
                                context.unbindService(connection)

                                throw ActivityNotFoundException()
                            }

                            state = State.Connecting(mutableListOf(action))
                        } catch (ex: ActivityNotFoundException) {
                            executor.submit { action(Result.failure(UnsupportedDialerException())) }
                        }
                    }
                }
            }
        }
    }

    private fun unbind() {
        synchronized(this) {
            val stateCopy = state

            if (stateCopy is State.Connecting || stateCopy is State.Connected) {
                context.unbindService(connection)

                state = State.Disconnected

                if (stateCopy is State.Connecting) stateCopy.listeners
                else emptyList()
            } else emptyList()
        }.also { list ->
            val result = Result.failure<WivewaDialerServer>(DeadObjectException())

            list.forEach { executor.submit { it(result) } }
        }
    }
}