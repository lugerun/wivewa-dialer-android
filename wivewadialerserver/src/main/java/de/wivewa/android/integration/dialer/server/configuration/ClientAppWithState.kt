/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.configuration

data class ClientAppWithState (
    val app: ClientApp,
    val state: State
) {
    companion object {
        fun calculate(installed: List<ClientApp>, allowed: List<ClientApp>): List<ClientAppWithState> {
            val installedByPackageName = installed.associateBy { it.packageName }
            val allowedByPackageName = allowed.groupBy { it.packageName }

            val processedInstalled = installedByPackageName.values.map { installedApp ->
                val allowedApps = allowedByPackageName[installedApp.packageName]
                val isAllowed = allowedApps?.any { it.doesMatchInstalledApp(installedApp) } ?: false
                val state =
                    if (isAllowed) State.Allowed
                    else if (allowedApps != null) State.Error.SignatureChanged
                    else State.Forbidden

                ClientAppWithState(
                    app = installedApp,
                    state = state
                )
            }

            val processedAllowed = (allowedByPackageName - installedByPackageName.keys).values.map { allowedApps ->
                ClientAppWithState(
                    app = allowedApps.first(),
                    state = State.Error.AppRemoved
                )
            }

            val allItems = (processedInstalled + processedAllowed)
                .sortedBy { it.app.packageName }
                .sortedBy { it.app.title }

            return allItems
        }
    }

    sealed class State {
        object Allowed: State()
        object Forbidden: State()

        sealed class Error: State() {
            object SignatureChanged: Error()
            object AppRemoved: Error()
        }
    }
}