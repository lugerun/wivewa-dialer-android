/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.configuration

import android.content.Context
import android.content.SharedPreferences
import android.os.Handler
import android.os.Looper

typealias ConfigurationChangeListener = (DialerServerConfiguration) -> Unit

class ConfigurationStorage (private val preferences: SharedPreferences) {
    companion object {
        private const val SUFFIX = ".de.wivewa.android.integration.dialer.server"
        private const val KEY = "config"

        private val lock = Object()
        private var instance: ConfigurationStorage? = null

        fun with(context: Context): ConfigurationStorage {
            return instance ?: synchronized(lock) {
                instance ?: ConfigurationStorage(
                    context.applicationContext.getSharedPreferences(
                        context.packageName + SUFFIX, Context.MODE_PRIVATE
                    )
                ).also { instance = it }
            }
        }
    }

    private val handler = Handler(Looper.getMainLooper())

    private var config: DialerServerConfiguration =
        preferences.getString(KEY, "")!!.let {
            if (it.isNotEmpty()) DialerServerConfiguration.parse(it)
            else DialerServerConfiguration.empty
        }

    private val configListeners = mutableSetOf<ConfigurationChangeListener>()

    fun getConfig() = config

    fun updateConfig(update: (DialerServerConfiguration) -> DialerServerConfiguration) = synchronized(this) {
        val newConfig = update(config)

        config = newConfig

        preferences.edit()
            .putString(KEY, newConfig.serialize())
            .apply()

        configListeners.forEach { listener ->
            handler.post { listener(newConfig) }
        }
    }

    fun addListener(listener: ConfigurationChangeListener): Unit = synchronized(this) { configListeners.add(listener) }
    fun removeListener(listener: ConfigurationChangeListener): Unit = synchronized(this) { configListeners.remove(listener) }
}