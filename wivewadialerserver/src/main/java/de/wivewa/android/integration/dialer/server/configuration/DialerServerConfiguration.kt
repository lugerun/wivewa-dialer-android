/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.configuration

import android.util.JsonReader
import android.util.JsonWriter
import java.io.StringReader
import java.io.StringWriter

data class DialerServerConfiguration (
    val allowedClients: List<ClientApp>
) {
    companion object {
        private const val ALLOWED_CLIENTS = "allowedClients"

        val empty = DialerServerConfiguration(
            allowedClients = emptyList()
        )

        fun parse(reader: JsonReader): DialerServerConfiguration {
            var allowedClients: List<ClientApp>? = null

            reader.beginObject()
            while (reader.hasNext()) {
                when (reader.nextName()) {
                    ALLOWED_CLIENTS -> allowedClients = mutableListOf<ClientApp>().also { list ->
                        reader.beginArray()
                        while (reader.hasNext()) {
                            list.add(ClientApp.parse(reader))
                        }
                        reader.endArray()
                    }
                    else -> reader.skipValue()
                }
            }
            reader.endObject()

            return DialerServerConfiguration(
                allowedClients = allowedClients!!
            )
        }

        fun parse(input: String) = JsonReader(StringReader(input)).use { parse(it) }
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()

        writer.name(ALLOWED_CLIENTS).beginArray()
        allowedClients.forEach { it.serialize(writer) }
        writer.endArray()

        writer.endObject()
    }

    fun serialize(): String = StringWriter().use {
        JsonWriter(it).use { serialize(it) }

        it.buffer.toString()
    }
}