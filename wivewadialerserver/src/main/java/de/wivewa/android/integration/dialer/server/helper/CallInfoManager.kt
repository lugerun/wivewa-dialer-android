/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.helper

import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.telecom.Call
import de.wivewa.android.integration.dialer.CallInfo

object CallInfoManager {
    fun getInfo(call: Call, callId: String): CallInfo = CallInfo().also { result ->
        result.id = callId
        result.remoteHandle = call.details.handle?.toString()
        result.handlePresentation = call.details.handlePresentation
        result.state = if (VERSION.SDK_INT >= VERSION_CODES.S) call.details.state else call.state
        result.direction = call.details.callDirection
        result.phoneAccount = call.details.accountHandle
    }
}
