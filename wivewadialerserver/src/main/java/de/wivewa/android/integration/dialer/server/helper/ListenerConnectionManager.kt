/*
 * wivewa-dialer-android - a dialer application for Android with APIs
 * Copyright (C) 2023 Jonas Lochmann
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see https://www.gnu.org/licenses/.
 */
package de.wivewa.android.integration.dialer.server.helper

import android.content.*
import android.content.pm.PackageManager.NameNotFoundException
import android.os.IBinder
import de.wivewa.android.integration.dialer.CallInfo
import de.wivewa.android.integration.dialer.DialerIntegrationActions
import de.wivewa.android.integration.dialer.WivewaDialerListener
import de.wivewa.android.integration.dialer.server.configuration.ClientApp
import de.wivewa.android.integration.dialer.server.configuration.ConfigurationChangeListener
import de.wivewa.android.integration.dialer.server.configuration.ConfigurationStorage
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ListenerConnectionManager (private val context: Context) {
    internal sealed class State {
        object Stopped: State()
        data class Running(
            val unprocessedAppListChanges: Boolean,
            val peerStatuses: Map<String, PeerStatus>
        ): State()
    }

    internal sealed class PeerStatus {
        abstract val connection: ServiceConnection?

        data class Connecting(val queuedItems: List<Message>, override val connection: ServiceConnection): PeerStatus()
        class Connected(val api: WivewaDialerListener, val executor: ExecutorService, override val connection: ServiceConnection): PeerStatus()
        object ConnectionBroken: PeerStatus() { override val connection: ServiceConnection? = null }
    }

    sealed class Message {
        class CallUpdated(val info: CallInfo): Message()
        class CallDone(val info: CallInfo): Message()
    }

    private var state: State = State.Stopped

    private val configurationStorage = ConfigurationStorage.with(context)

    private val configurationChangeListener: ConfigurationChangeListener = { reportChange() }
    private val configurationChangeReceiver = object: BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            reportChange()
        }
    }

    fun start(): Unit = synchronized(this) {
        if (state == State.Stopped) {
            configurationStorage.addListener(configurationChangeListener)

            context.registerReceiver(configurationChangeReceiver, IntentFilter(Intent.ACTION_PACKAGE_ADDED))
            context.registerReceiver(configurationChangeReceiver, IntentFilter(Intent.ACTION_PACKAGE_REMOVED))

            state = State.Running(
                unprocessedAppListChanges = true,
                peerStatuses = emptyMap()
            )
        }
    }

    fun stop(): Unit = synchronized(this) {
        val oldState = state

        if (oldState is State.Running) {
            configurationStorage.removeListener(configurationChangeListener)
            context.unregisterReceiver(configurationChangeReceiver)

            oldState.peerStatuses.values.forEach {
                it.connection?.let { context.unbindService(it) }

                if (it is PeerStatus.Connected) it.executor.shutdown()
            }

            state = State.Stopped
        }
    }

    fun send(message: Message) = synchronized(this) {
        updateConnections()

        val currentState = state.let {
            if (it is State.Running) it
            else return
        }

        state = currentState.copy(
            peerStatuses = currentState.peerStatuses.mapValues { (_, peerStatus) ->
                when (peerStatus) {
                    is PeerStatus.Connecting -> peerStatus.copy(
                        queuedItems = peerStatus.queuedItems + message
                    )
                    is PeerStatus.Connected -> peerStatus.also { it.executor.submit {
                        dispatchSync(peerStatus.api, message)
                    } }
                    is PeerStatus.ConnectionBroken -> peerStatus
                }
            }
        )
    }

    private fun reportChange() {
        synchronized(this) {
            val oldState = state

            if (oldState is State.Running) {
                state = oldState.copy(unprocessedAppListChanges = true)
            }
        }
    }

    private fun updateConnections() {
        synchronized(this) {
            var currentState = state.let {
                if (it is State.Running) it
                else return
            }

            try {
                if (currentState.unprocessedAppListChanges) {
                    val validClients = configurationStorage.getConfig().allowedClients.filter {
                        try {
                            it.doesMatchInstalledApp(
                                ClientApp.fromPackage(
                                    context.packageManager,
                                    it.packageName
                                )
                            )
                        } catch (ex: NameNotFoundException) {
                            false
                        }
                    }

                    val validClientPackageNames = validClients.map { it.packageName }.toSet()

                    kotlin.run {
                        // close obsolete connections
                        val removedClientPackages =
                            currentState.peerStatuses.filter { !validClientPackageNames.contains(it.key) }

                        if (removedClientPackages.isNotEmpty()) {
                            currentState = currentState.copy(
                                peerStatuses = currentState.peerStatuses.filterKeys {
                                    !removedClientPackages.contains(
                                        it
                                    )
                                }
                            )

                            removedClientPackages.values.forEach {
                                it.connection?.let { context.unbindService(it) }

                                if (it is PeerStatus.Connected) it.executor.shutdown()
                            }
                        }
                    }

                    kotlin.run {
                        // open new connections
                        val newPackageNames = validClientPackageNames - currentState.peerStatuses.keys

                        newPackageNames.forEach { newPackageName ->
                            currentState = currentState.copy(
                                peerStatuses = currentState.peerStatuses + Pair(newPackageName, createServiceConnection(newPackageName))
                            )
                        }
                    }
                }
            } finally {
                state = currentState
            }
        }
    }

    private fun dispatchSync(api: WivewaDialerListener, message: Message) {
        try {
            when (message) {
                is Message.CallUpdated -> api.onCallUpdated(message.info)
                is Message.CallDone -> api.onCallEnded(message.info)
            }
        } catch (ex: Exception) {
            // ignore
        }
    }

    private fun createServiceConnection(packageName: String): PeerStatus {
        val connection = object: ServiceConnection {
            override fun onServiceConnected(p0: ComponentName?, binder: IBinder) {
                val api = WivewaDialerListener.Stub.asInterface(binder)

                synchronized(this@ListenerConnectionManager) {
                    val oldState = state

                    if (oldState is State.Running) {
                        val oldPeer = oldState.peerStatuses[packageName]

                        if (oldPeer != null && oldPeer is PeerStatus.Connecting) {
                            val executor = Executors.newSingleThreadExecutor()

                            oldPeer.queuedItems.forEach { item ->
                                executor.submit { dispatchSync(api, item) }
                            }

                            state = oldState.copy(
                                peerStatuses = oldState.peerStatuses + Pair(
                                    packageName,
                                    PeerStatus.Connected(
                                        executor = executor,
                                        connection = this,
                                        api = api
                                    )
                                )
                            )
                        } else context.unbindService(this)
                    } else context.unbindService(this)
                }
            }

            override fun onNullBinding(name: ComponentName?) {
                super.onNullBinding(name)

                synchronized(this@ListenerConnectionManager) {
                    val oldState = state

                    if (oldState is State.Running) {
                        val oldPeer = oldState.peerStatuses[packageName]

                        if (oldPeer != null && oldPeer is PeerStatus.Connecting) {
                            state = oldState.copy(
                                peerStatuses = oldState.peerStatuses + Pair(
                                    packageName,
                                    PeerStatus.ConnectionBroken
                                )
                            )

                            context.unbindService(this)
                        }
                    }
                }
            }

            override fun onServiceDisconnected(p0: ComponentName?) {
                synchronized(this@ListenerConnectionManager) {
                    val oldState = state

                    if (oldState is State.Running) {
                        val oldPeer = oldState.peerStatuses[packageName]

                        if (oldPeer != null && oldPeer is PeerStatus.Connected) {
                            state = oldState.copy(
                                peerStatuses = oldState.peerStatuses + Pair(
                                    packageName,
                                    PeerStatus.Connecting(emptyList(), this)
                                )
                            )
                        }
                    }
                }
            }

            override fun onBindingDied(name: ComponentName?) {
                super.onBindingDied(name)

                synchronized(this@ListenerConnectionManager) {
                    val oldState = state

                    if (oldState is State.Running) {
                        val oldPeer = oldState.peerStatuses[packageName]

                        if (oldPeer is PeerStatus.Connecting || oldPeer is PeerStatus.Connected) {
                            state = oldState.copy(
                                peerStatuses = oldState.peerStatuses + Pair(
                                    packageName,
                                    PeerStatus.ConnectionBroken
                                )
                            )

                            context.unbindService(this)
                        }
                    }
                }
            }
        }

        val intent = Intent(DialerIntegrationActions.ACTION_LISTENER_SERVICE)
            .setPackage(packageName)

        try {
            if (!context.bindService(intent, connection, Context.BIND_AUTO_CREATE)) {
                context.unbindService(connection)

                throw ActivityNotFoundException()
            }

            return PeerStatus.Connecting(
                queuedItems = emptyList(),
                connection = connection
            )
        } catch (ex: ActivityNotFoundException) {
            return PeerStatus.ConnectionBroken
        }
    }
}